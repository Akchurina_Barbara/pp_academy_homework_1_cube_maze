using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "LevelsData", menuName = "ScriptableObjects/Levels data", order = 1)]
public class LevelsDescription : ScriptableObject
{
    public float Timer;
    public GameObject levelPrefub;

}
