using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AppContext
{
    private static ResourceManager _resourceManager;

    public static ResourceManager ResourceManager { get { return _resourceManager; } }


    public static void Configure()
    {
        _resourceManager = new ResourceManager();
    }
}