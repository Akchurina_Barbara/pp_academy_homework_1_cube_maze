using System.Collections;
using System.Collections.Generic;
using UnityEditor.SearchService;
using UnityEngine;

public class DoorAutoTimer : MonoBehaviour
{
    [Header("Timer")]
    [SerializeField] private float _closingTimer;
    [SerializeField] private float _openningTimer;

    [Header("Objects")]
    [SerializeField] private GameObject _doors;

    private bool _isClose = true;

    private void Awake()
    {
        StartCoroutine(SwitchState(_closingTimer));
    }

    private IEnumerator SwitchState(float time)
    {
        yield return new WaitForSeconds(time);

        _isClose = !_isClose;
        _doors.SetActive(_isClose);
        StartCoroutine(SwitchState(_isClose? _closingTimer : _openningTimer));
    }
}
 