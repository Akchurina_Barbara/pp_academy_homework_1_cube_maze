using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    [Header("Shoot")]
    [SerializeField] private float _shootDelay;
    [SerializeField] private GameObject _bullet;
    [SerializeField] private Transform _shootPoint;

    [Header("Observable")]
    [SerializeField] private Transform[] _observablePoints;
    [SerializeField] private float _lookDelay;

    [Header("Field of View")]
    [SerializeField] private GameObject _fieldOfView;
    [SerializeField] private LayerMask _obstacleMask;
    [SerializeField] private LayerMask _playerMask;

    private float _lookTimer = 0f;
    private float _shootTimer = 0f;
    private int _currentObservablePointIndex = 0;

    private void Awake()
    {
        _lookTimer = _lookDelay;
        _shootTimer = 0;
    }

    private void Update()
    {
        LookAtTimerTick();
        LookAtPoint(_currentObservablePointIndex);
    }

    private void LookAtTimerTick()
    {
        _lookTimer -= Time.deltaTime;

        if (_lookTimer <= 0)
        {
            int nextPointIndex = _currentObservablePointIndex + 1;

            if (nextPointIndex >= _observablePoints.Length)
                nextPointIndex = 0;

            _currentObservablePointIndex = nextPointIndex;
            _lookTimer = _lookDelay;
        }
    }

    private void Shoot()
    {
        _shootTimer -= Time.deltaTime;

        if (_shootTimer <= 0)
        {
            Instantiate(_bullet, _shootPoint);
            _shootTimer = _shootDelay;
        }
    }

    private void LookAtPoint(int pointIndex)
    {
        Vector3 distanceToPoint = _observablePoints[pointIndex].transform.position - transform.position;
        Vector3 directionToPoint = distanceToPoint.normalized;

        if (Physics.Raycast(transform.position, directionToPoint, out RaycastHit hit, distanceToPoint.magnitude, _playerMask))
        {
            _lookTimer = _lookDelay;
            Shoot();
        }
        else
        {
            _shootTimer = 0;
        }

        _fieldOfView.SetActive(Physics.Raycast(transform.position, directionToPoint, distanceToPoint.magnitude, _obstacleMask) == false);
        transform.forward = new Vector3(directionToPoint.x, 0f, directionToPoint.z);
    }
}
