using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorKeyOpen : MonoBehaviour
{
    [SerializeField] private DoorAutoTrigger _doorTrigger;
    [SerializeField] private DoorKey _key;

    [Header("Objects")]
    [SerializeField] private GameObject _doors;

    [Header("Render")]
    [SerializeField] Renderer _renderer;

    [SerializeField] private Material _disableMaterial;
    [SerializeField] private Material _enableMaterial;

    private bool _canBeOpen;

    private void SetMaterial(Material material)
    {
        _renderer.material = material;
        _doorTrigger.SetMaterial(material);
    }

    private void Awake()
    {
        _doorTrigger.OnPlayerTriggerEnter += Open;
        _doorTrigger.OnPlayerTriggerExit += Close;
        _key.OnPlayerKeyHold += KeyHold;
    }

    private void Start()
    {
        SetMaterial(_disableMaterial);
        _key.SetMaterial(_enableMaterial);
    }

    private void OnDestroy()
    {
        _doorTrigger.OnPlayerTriggerEnter -= Open;
        _doorTrigger.OnPlayerTriggerExit -= Close;
        _key.OnPlayerKeyHold += KeyHold;
    }

    private void KeyHold()
    {
        _canBeOpen = true;
        SetMaterial(_enableMaterial);
    }

    private void Open()
    {
        if (_canBeOpen)
        {
            _doors.SetActive(false);
        }
    }

    private void Close()
    {
        if (_canBeOpen)
        {
            _doors.SetActive(true);
        }
    }
}
