using System.Collections;
using System.Collections.Generic;
using UnityEditor.SearchService;
using UnityEngine;

public class MovingObstacle : MonoBehaviour
{
    [Header("Timer")]
    [SerializeField] private float _upTimer;
    [SerializeField] private float _downTimer;

    [Header("Objects")]
    [SerializeField] private GameObject _obstacle;

    private bool _isUp = true;

    private void Awake()
    {
        StartCoroutine(SwitchState(_upTimer));
    }

    private IEnumerator SwitchState(float time)
    {
        yield return new WaitForSeconds(time);

        _isUp = !_isUp;
        _obstacle.SetActive(_isUp);
        StartCoroutine(SwitchState(_isUp? _upTimer : _downTimer));
    }
}
 