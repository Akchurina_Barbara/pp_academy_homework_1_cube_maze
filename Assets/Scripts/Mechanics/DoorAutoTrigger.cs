using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorAutoTrigger : MonoBehaviour
{
    [SerializeField] Renderer[] _renderers;

    public Action OnPlayerTriggerEnter;
    public Action OnPlayerTriggerExit;

    public void SetMaterial(Material material)
    {
        foreach (var renderer in _renderers)
        {
            renderer.material = material;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player player))
        {
            OnPlayerTriggerEnter?.Invoke();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent(out Player player))
        {
            OnPlayerTriggerExit?.Invoke();
        }
    }
}
