using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    [SerializeField] private Teleport _nextTeleport;

    public bool IsSender { get; set; } = true;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player player) 
            && IsSender && _nextTeleport)
        {
            _nextTeleport.IsSender = false;
            player.gameObject.SetActive(false);
            player.transform.position = _nextTeleport.transform.position;
            player.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.TryGetComponent(out Player player))
        {
            IsSender = true;
        }
    }
}
