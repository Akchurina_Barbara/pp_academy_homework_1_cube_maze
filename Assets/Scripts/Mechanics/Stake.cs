using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Stake : MonoBehaviour
{
    [SerializeField] private Transform _pointStart;
    [SerializeField] private Transform _pointEnd;
    [SerializeField] private float _speed;
    [SerializeField] private float _startDelay;
    [SerializeField] private Transform _movingObjectTransform;
    [SerializeField] private Rigidbody _movingObjectRigidbody;

    private float _startTimer;

    private Coroutine _changeDirectionCoroutine;

    private void Start()
    {
        _startTimer = _startDelay; 
    }

    private IEnumerator ChangeDirection(Vector3 direction, bool timeDelay)
    {
        _movingObjectRigidbody.velocity = Vector3.zero;

        if (timeDelay)
        {
            yield return new WaitForSeconds(_startDelay);
        }

        _movingObjectRigidbody.velocity = direction * _speed;

        yield return new WaitForFixedUpdate();
        _changeDirectionCoroutine = null;
    }

    private void FixedUpdate()
    {
        if (_changeDirectionCoroutine == null)
        {
            if (_movingObjectTransform.position == _pointStart.position)
            {
                _changeDirectionCoroutine = StartCoroutine(ChangeDirection(transform.right, true));
            }
            else if (_movingObjectTransform.position == _pointEnd.position)
            {
                _changeDirectionCoroutine = StartCoroutine(ChangeDirection(-transform.right, false));
            }
        }
    }
}
