using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.Experimental.GraphView.GraphView;

public class CellFalling : MonoBehaviour
{
    [SerializeField] private float _destroyDelay;
    [SerializeField] private Renderer _renderer;
    [SerializeField] private Material _defaultMaterial;
    [SerializeField] private Material _activateMaterial;

    private Player _player;

    private void Start()
    {
        _renderer.material = _defaultMaterial;
    }

    private IEnumerator DetroyCell()
    {
        yield return new WaitForSeconds(_destroyDelay);
        if (_player) 
        { 
            _player.Kill(); 
        }

        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player player))
        {
            _player = player;
            _renderer.material = _activateMaterial;
            StartCoroutine(DetroyCell());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent(out Player player))
        {
            _player = null;
        }
    }
}
