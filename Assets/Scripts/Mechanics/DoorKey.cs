using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorKey : MonoBehaviour
{
    [SerializeField] Renderer _renderer;

    public Action OnPlayerKeyHold;

    public void SetMaterial(Material material)
    {
        _renderer.material = material;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player player))
        {
            OnPlayerKeyHold?.Invoke();
            Destroy(gameObject);
        }
    }
}
