using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class CellMoving : MonoBehaviour
{
    [SerializeField] private Transform[] _movingPoints;
    [SerializeField] private float _moveDelay;

    private float _timer = 0;
    private int _currentPatrolPointIndex = 0;

    private Player _player;

    private void Update()
    {
        PatrolTimerTick();
        MoveToPoint(_currentPatrolPointIndex);
    }

    private void PatrolTimerTick()
    {
        _timer -= Time.deltaTime;

        if (_timer <= 0)
        {
            int nextPointIndex = _currentPatrolPointIndex + 1;

            if (nextPointIndex >= _movingPoints.Length)
                nextPointIndex = 0;

            _currentPatrolPointIndex = nextPointIndex;
            _timer = _moveDelay;
        }
    }

    private void MoveToPoint(int pointIndex)
    {
        transform.position = new Vector3(_movingPoints[pointIndex].position.x, transform.position.y, _movingPoints[pointIndex].position.z);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Movement player))
        {
            player.RoutePoint = transform;
        }
    }
}
