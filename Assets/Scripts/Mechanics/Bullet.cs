using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float _autoDeathTime;
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private float _speed;
    [SerializeField] private float _damage;

    private IEnumerator AutoDeath()
    {
        yield return new WaitForSeconds(_autoDeathTime);
    }

    private void Start()
    {
        _rigidbody.AddForce(transform.forward * _speed, ForceMode.VelocityChange);

        StartCoroutine("AutoDeath");
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Health player))
        {
            player.TakeDamage(_damage);
        }

        Destroy(gameObject);
    }
}
