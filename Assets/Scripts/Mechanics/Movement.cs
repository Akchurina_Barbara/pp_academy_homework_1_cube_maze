﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.Experimental.GraphView.GraphView;

public class Movement : MonoBehaviour
{
    [SerializeField] private LayerMask _obstacleMask;
    [SerializeField] private LayerMask _cellMask;
    [SerializeField] private float _step;

    public Transform RoutePoint { private get; set; }
    private void Update()
    {
        if (RoutePoint)
        {
            transform.position = new Vector3(RoutePoint.position.x, transform.position.y, RoutePoint.position.z);
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
            TryMove(Vector3.forward);
        
        if(Input.GetKeyDown(KeyCode.DownArrow))
            TryMove(Vector3.back);
        
        if(Input.GetKeyDown(KeyCode.RightArrow))
            TryMove(Vector3.right);
        
        if(Input.GetKeyDown(KeyCode.LeftArrow))
            TryMove(Vector3.left);
    }

    private void TryMove(Vector3 direction)
    {
        var forwardRay = new Ray(transform.position, direction);

        if (Physics.Raycast(forwardRay, out RaycastHit hit, _step, _obstacleMask))
            return;

        var downRay = new Ray(direction * _step  + transform.position, (Vector3.down - direction) * _step);

        if (!Physics.Raycast(downRay, out RaycastHit hit2, _step, _cellMask))
            return;

        transform.forward = direction;
        transform.Translate(direction * _step, Space.World);

        RoutePoint = null;
    }
}
