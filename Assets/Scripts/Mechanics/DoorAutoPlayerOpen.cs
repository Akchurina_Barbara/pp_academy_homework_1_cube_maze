using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DoorAutoPlayerOpen : MonoBehaviour
{
    [SerializeField] private DoorAutoTrigger _doorTrigger;

    [Header("Objects")]
    [SerializeField] private GameObject _doors;

    [Header("Render")]
    [SerializeField] Renderer _renderer;
    [SerializeField] private Material _material;

    private void Awake()
    {
        _doorTrigger.OnPlayerTriggerEnter += Open;
        _doorTrigger.OnPlayerTriggerExit += Close;

    }

    private void Start()
    {
        _renderer.material = _material;
        _doorTrigger.SetMaterial(_material);
    }

    private void OnDestroy()
    {
        _doorTrigger.OnPlayerTriggerEnter -= Open;
        _doorTrigger.OnPlayerTriggerExit -= Close;
    }

    private void Open()
    {
        _doors.SetActive(false);
    }

    private void Close()
    {
        _doors.SetActive(true);
    }
}
