﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static UnityEditor.Experimental.GraphView.GraphView;

public class Game : MonoBehaviour
{
    [Header("Test")]
    [SerializeField] private bool _levelSwitchDisable;


    [Header("Timer")]
    [SerializeField] private bool _timerIsOn;
    [SerializeField] private float _nextLevelDelay;
    [SerializeField] private Text _timerView;

    [Header("Health")]
    [SerializeField] private Text _healthText;

    [Header("UI")]
    [SerializeField] private ResultScreenController _resultScreen;


    private Player _player;
    private Health _playerHealth;
    private Exit _exitFromLevel;

    private float _timer = 0;
    private bool _gameIsEnded = false;

    private int _currentLevel;


    private void Start()
    {
        _currentLevel = PlayerPrefs.GetInt("CurrentLevel");
        GameObject.Instantiate(AppContext.ResourceManager.LevelDescriptions[_currentLevel].levelPrefub);

        _timer = AppContext.ResourceManager.LevelDescriptions[_currentLevel].Timer;

        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        _playerHealth = _player.gameObject.GetComponent<Health>();
        _exitFromLevel = GameObject.FindGameObjectWithTag("Exit").GetComponent<Exit>();
        _exitFromLevel.Close();
    }

    private IEnumerator NextLevel()
    {
        yield return new WaitForSeconds(_nextLevelDelay);
        SceneManager.LoadScene("game");
    }

    private void Update()
    {
        if(_gameIsEnded)
            return;
        
        TimerTick();
        LookAtPlayerHealth();
        LookAtPlayerInventory();
        TryCompleteLevel();
    }

    private void TimerTick()
    {
        if(_timerIsOn == false)
            return;
        
        _timer -= Time.deltaTime;
        _timerView.text = $"{_timer:F1}";
        
        if(_timer <= 0)
            Lose();
    }

    private void TryCompleteLevel()
    {
        if(_exitFromLevel.IsOpen == false)
            return;

        var flatExitPosition = new Vector2(_exitFromLevel.transform.position.x, _exitFromLevel.transform.position.z);
        var flatPlayerPosition = new Vector2(_player.transform.position.x, _player.transform.position.z);

        if (flatExitPosition == flatPlayerPosition)
            Victory();
    }

    private void LookAtPlayerHealth()
    {
        _healthText.text = $"{_playerHealth.CurrentHealth}";

        if(_player.IsAlive)
            return;

        Death();
        Destroy(_player.gameObject);
    }

    private void LookAtPlayerInventory()
    {
        if(_player.HasKey)
            _exitFromLevel.Open();
    }

    public void Victory()
    {
        _gameIsEnded = true;
        _player.Disable();
        _resultScreen.ShowComplete();

        if (!_levelSwitchDisable)
        {
            if (_currentLevel < AppContext.ResourceManager.LevelDescriptions.Count - 1)
            {
                _currentLevel++;
            }
            else
            {
                _currentLevel = 0;
            }
        }

        PlayerPrefs.SetInt("CurrentLevel", _currentLevel);
        StartCoroutine("NextLevel");
    }

    public void Lose()
    {
        _gameIsEnded = true;
        _player.Disable();
        _resultScreen.ShowTimeOver();
        StartCoroutine("NextLevel");
    }

    public void Death()
    {
        _gameIsEnded = true;
        _player.Disable();
        _resultScreen.ShowDeath();
        StartCoroutine("NextLevel");
    }
}
