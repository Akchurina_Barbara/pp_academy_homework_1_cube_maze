using CommonTypes;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class ResourceManager /*: MonoBehaviour*/
{
    private Dictionary<int, LevelsDescription> _levelDescriptions;

    public Dictionary<int, LevelsDescription> LevelDescriptions { get { return _levelDescriptions; } }

    private void LoadAll()
    {
        var levelsArray = Resources.LoadAll<LevelsDescription>(StringConstants.LevelsPath);
        _levelDescriptions = new Dictionary<int, LevelsDescription > ();

        for (int i = 0; i < levelsArray.Length; i++)
        {
            _levelDescriptions[i] = levelsArray[i];
        }
    }

    public ResourceManager()
    {
        LoadAll();
    }
}
