using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ResultScreenController : MonoBehaviour
{
    [Header("Objects")]
    [SerializeField] private TextMeshProUGUI _text;
    [SerializeField] private Image _background;


    [Header("Win")]
    [SerializeField] private Color _winTextColor;
    [SerializeField] private Color _winBackgroundColor;


    [Header("Lose")]
    [SerializeField] private Color _loseTextColor;
    [SerializeField] private Color _loseBackgroundColor;

    private void Awake()
    {
        gameObject.SetActive(false);
    }

    public void ShowComplete()
    {        
        _text.text = "Mission COMPLETE!";
        _text.color = _winTextColor;
        _background.color = _winBackgroundColor;
        gameObject.SetActive(true);
    }

    public void ShowTimeOver()
    {
        _text.text = "Time over!";
        _text.color = _loseTextColor;
        _background.color = _loseBackgroundColor;
        gameObject.SetActive(true);
    }

    public void ShowDeath()
    {
        _text.text = "You DEAD!";
        _text.color = _loseTextColor;
        _background.color = _loseBackgroundColor;
        gameObject.SetActive(true);
    }
}
